=== Toy QuickTags ===
Contributors: thewhodidthis
Tags: qtags
Requires at least: 3.0.1
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Adds missing quicktags for `<span>`, `<small>`, and `<hr>`.
